
* [Site généré en ligne](https://hackadon.frama.io/hackadon2019-site-web/)
* [Site en local](http://localhost:1111/)


* [Starter de base utilisé](https://github.com/hasura/gatsby-gitbook-starter)

# A faire : 

* page "les partenaires"

# gestion des menus de navigation 

* codé manuellement dans le template actuel 
* un sympas, mais peut maintenu, mais dispose de la fonction onclick https://github.com/Lapple/react-json-inspector
* un simple : https://github.com/chenglou/react-treeview
* un simple et propre, maitenance à valider : https://github.com/OpusCapita/react-tree-component
* un avec material-ui une cible intéressante : https://material-ui.com/components/tree-view/


* Notes sur le modification du json-inspector : 
  * navigation en plein écran : `gatsby/src/components/layout.js`
  * navigation en taille réduite (non modifié) : `gatsby/src/components/Header.js`


# Autres notes : 

* L'image Docker de base est passé à node:buster car node:alpine pose des pbs pour une dépendance gatsby.


* A revoir, la propriété word-count pour chaque mdx [lien](http://localhost:1111/___graphql?query=query%20MyQuery%20%7B%0A%20%20allMdx%20%7B%0A%20%20%20%20edges%20%7B%0A%20%20%20%20%20%20node%20%7B%0A%20%20%20%20%20%20%20%20frontmatter%20%7B%0A%20%20%20%20%20%20%20%20%20%20metaTitle%0A%20%20%20%20%20%20%20%20%20%20title%0A%20%20%20%20%20%20%20%20%20%20metaDescription%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20fields%20%7B%0A%20%20%20%20%20%20%20%20%20%20id%0A%20%20%20%20%20%20%20%20%20%20slug%0A%20%20%20%20%20%20%20%20%20%20title%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20wordCount%20%7B%0A%20%20%20%20%20%20%20%20%20%20paragraphs%0A%20%20%20%20%20%20%20%20%20%20sentences%0A%20%20%20%20%20%20%20%20%20%20words%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D%0A)