
* frigo : 
 * publication claire de donnée, bonne géolocalisation
 * cartographie

* engagement : 
 * a préciser
 * diversité d’exprimer des besoins de tous types (besoins de mises en relation, d’appui ponctuel, de conseils ciblés, d’informations…) 


* Alerter contre l'obésite
 * data + interaction

* FSHD Care
 * choisir entre : 
   * diagnostic 
   * accompagnement de chaque malade sur sa pratique d’activités physiques, sur sa gestion de la nutrition.
   * recencement de tous les acteurs offrant un service pertinent pour les malades.
   * capting IOT

*  Pass'Sport pour l'Emploi 
  * module de formation "sport et performance" : entièrement gratuit et qui s'étend sur 12 semaines. 
    * sur les compétences, avec emission de badges
    * publication rdfa des contenus de formation
    * bilan de compétence à l'entrée et à la sortie


*  Alerter contre l'obésite 
  * a sélectionner parmis : 
  	* un volet nutrition (repas: qu’avez-vous manger ?), 
  	* un volet sport/mobilité (avez-vous bouger votre corps ?), 
  	* un volet bien-être général (avez-vous bien dormi ? êtes-vous stressé par votre vie ? 

