---
title: "Evaluation"
metaTitle: "Evaluation"
metaDescription: "Evaluation"
---

#### Rappel des livrables :

* Dossier de la stratégie proposée (objectifs, cibles, enjeux, moyens, KPI …) 
* Documentation technique de la solution proposée (schémas, glossaire et liens utiles) avec dépôt Git
* Réalisation du branding complet + prototype HD (message, branding graphique, user flow, déclinaison maquettes …)
* Réalisation d'une présentation argumentée de 10 minutes pour le pitch de fin



#### Vous trouverez ci-dessous les critères sur lesquels se basera l'évaluation de votre projet.

<iframe class="googledoc-iframe" src="https://docs.google.com/document/d/e/2PACX-1vS47s8rTrXeAC3kyCoYIQqSJLyg8L_5mZw3Nh2lC1jrW-x4Xy_ZOT54Hns_QIY4zzL-FK-4TLJ3ghbV/pub?embedded=true"></iframe>