---
title: 'Ateliers'
metaTitle: 'Ateliers'
metaDescription: 'Description des ateliers'
---

Nous aurons deux sessions d'ateliers le mercredi. Une première le matin de **11h30 à 12h30** et une deuxième en début d'après-midi de **14h00 à 15h00** avec Google et microDON.

Voici le programme proposé pour les différents ateliers par session :   

## Google  

* Description générale de Google cloud Platform.
* Atelier technique de démonstration.

## microDON  (PFE)

* Démonstration de la plateforme de l'engagement  
* Projection dans les cas d'usages liés aux API mises à disposition par microDON  

## microDON (Écologie et numérique)

Quels impacts écologiques a le numérique ? Comment les réduire ? Quelles sont les bonnes pratiques à adopter en entreprise ? Qu'est-ce que l'éco-conception de service numérique ? 
Éléments de réponse suivi d’un atelier pratique pour estimer l’impact d’un site internet.
