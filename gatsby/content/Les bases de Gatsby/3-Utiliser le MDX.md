---
title: 'Markdown + JSX'
metaTitle: "Markdown + JSX"
metaDescription: 'Introduction au Markdown + JSX'
---
import Toaster from '../../src/components/toast'

Le format Markdown permet d'écrire très facilement des pages et de les convertir au format HTML. Mais comment faire si on veut inclure à notre page des composants plus complexes ? C'est là que le format MDX rentre en jeu ! MDX signifie Markdown + JSX, c'est à dire qu'il mêle la simplicité du Markdown et la puissance des composants React. 

## Exemple 1 - toaster

Prenons un exemple tout simple : inclure un bouton affichant une notification à notre page. Pour ça, on va utiliser le composant [react-toastify](https://github.com/fkhadra/react-toastify).

Première étape : installer react-toastify.

```bash
cd [dossier_local_du_repository]
npm install react-toastify
```

Voilà. On a les fichiers nécessaires pour utiliser ce composant. Il faut désormais l'inclure à notre projet. Pour cela, on va créer un nouveau composant dans `gatsby/src/components`. Appelons-le `toast.js`.

```bash
cd gatsby/src/components
touch toast.js
```

Dans ce fichier, on importe les modules nécessaires et écrit le code nécessaire à l'affichage d'une notification

```jsx
import React, { Component } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default class Toaster extends Component {
	notify = () => toast("Wow so easy !");

	render(){
  		return (
    		<div>
      			<button onClick={this.notify}>Notify !</button>
      			<ToastContainer />
    		</div>
  		);
	}
}
```

Voilà. Notre composant est prêt, il n'y a plus qu'à l'inclure dans une page écrite en MDX.

```bash
cd gatsby/content/
touch toaster.md
```

Dans cette page, j'écris tout simplement :

```mdx
---
title: 'Exemple MDX : Toaster'
metaTitle: "Exemple MDX : Toaster"
metaDescription: 'Utiliser le format MDX - Exemple du Toaster'
---
import Toaster from '../../src/components/toast'

Inclure un composant React dans du MDX, c'est **facile** !!

<Toaster />
```

Et voilà le [résultat](./Exemple%201%20-%20toaster) !

<Toaster />

## Exemple 2 - récupérer et afficher des depuis une API

Un autre exemple ? Regardons comment récupérer une liste d'images depuis https://picsum.photos/

### Le composant

```javascript
import React, { Component } from 'react';

export default class FetchApiExample extends Component {

    constructor(props) {
        super(props);

        this.baseUrl = "https://picsum.photos";
        this.apiUrl = "/v2/list?limit=8";

        this.state = {
            images: null,
        }
    }

    componentDidMount() {
        fetch(this.baseUrl + this.apiUrl )
            .then(response => response.json()
                .then(images => {
                    this.setState({images});
                }));
    }


    render(){
        if (!this.state.images) return <p>Loading...</p>;

        const images = this.state.images;

        return (
            <section className={"projectWrapper"}>
                {  images.map( ({author, url, id,}) =>
                    <div key={id} className={"projectCard"}>
                        <a href={url}>
                            <img src={this.baseUrl + "/id/" + id + "/250/300.jpg"} alt={"Image de " + author} />
                            <p>Auteur : {author}</p>
                        </a>
                    </div>
                )}
            </section>
        );
    }

}
```

### La page en mdx

```markdown
---
title: 'Exemple Fetch API'
metaTitle: "Exemple Fetch API"
metaDescription: "Comment utiliser javascript pour fetch l'API de la PFE et l'incorporer dans Gatsby"
---
import FetchPfeExample from '../../src/components/FetchApiExample'

Des images au hasard :

<FetchApiExample />
```

### Un peu de CSS

```css
.projectWrapper {
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  column-gap: 10px;
}
```

### Et...

...voilà le [résultat](./Exemple%202%20-%20Fetch%20une%20API) !

