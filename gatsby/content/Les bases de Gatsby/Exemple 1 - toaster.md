---
title: 'Exemple MDX : Toaster'
metaTitle: "Exemple MDX : Toaster"
metaDescription: 'Utiliser le format MDX - Exemple du Toaster'
---
import Toaster from '../../src/components/toast'

Inclure un composant React dans du MDX, c'est **facile** !!

<Toaster />

