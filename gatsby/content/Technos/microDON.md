
# microDON : donner un peu, plus souvent

microDON est une société de l'économie sociale et solidaire qui propose aux enseignes et aux entreprises des solutions de don au profit d'associations : 

- l'ARRONDI en caisse permet d'arrondir ses achats à l'euro supérieur dans les magasins partenaires, directement en caisse ou sur les terminaux de paiement électroniques
- l'ARRONDI sur salaire permet de donner tous les mois les centimes de sa fiche de paie, avec des dons doublés par l'employeur
- le don de temps, disponible sur [la plateforme de l'engagement](https://engagement-hackadon-2019.micropfs.space/hackadon2019) permet de participer à des missions solidaires variées 

## La plateforme de l'engagement

Ce guichet unique de l'engagement rassemble tous les programmes solidaires d'une entreprise. On peut notamment y créer des missions auxquelles les salariés peuvent s'inscrire.  
Developpée selon le principe API-first, elle permet de créer des programmes, des missions, d'inviter des utilisateurs ou de leur attribuer des rôles à travers une interface, mais aussi directement par API.  
La plateforme dispose de pages publiques accessibles à tous, et d'un back-office où les responsables peuvent gérer les missions solidaire, valider les inscriptions, rédiger le bilan des missions terminées, etc.  
  
Les programmes proposés peuvent être de différents types :
  - Don de temps : on peut y associer des missions 
  - Page libre : contenu texte libre, inclusion de liens, d'iframe, etc.
  - Don sur salaire : réservé aux entreprises proposant le don sur salaire à leurs collaborateurs

Les programmes créés sont par défaut en mode brouillon. Ils peuvent être modifiés et publiés (soit via un wysiwyg, soit par API) à condition de disposer des autorisations suffisantes. Un programme terminé peut également être archivé.  

Les missions doivent comporter des paramètres obligatoires : dates, adresse, association bénéficiaire, nombre de places.  
Comme les programmes, elles sont créées en mode brouillon et peuvent être publiées et archivées. Une mission publiée et non-archivée peut recevoir des inscriptions de la part des volontaires.


## Les API microDON

- l'API de la plateforme de l'engagement permet de manipuler *notamment* les objets Program, Event et Inscription (GET uniquement)
- La documentation auto-générée de la totalité de l'API se trouve [ici](https://api-hackadon-2019.micropfs.space)
- pour requêter ces endpoints, vous pouvez utiliser directement la page ci-dessus, [Postman](https://www.getpostman.com/apps) ou vous inspirer de l'[exemple du fetch sur Gatsby](../Les%20bases%20de%20Gatsby/3-Utiliser%20le%20MDX.md).

**À NOTER** : Pour une question de cors, l'API de microDON n'est actuellement accessible que depuis un environnement local. Si vous souhaitez absolument l'utiliser en ligne, rapprochez-vous d'un dev de microDON et on verra ce qu'il est possible de faire.


Plus d'infos sur [microDON](https://microdon.org/) ou [l'ARRONDI](https://larrondi.org/)

