---
title: 'OpenBadges - Présentation'
metaTitle: 'OpenBadges - Présentation'
metaDescription: 'OpenBadges - Présentation'
authors : 
	- "Valentin GUILBAUD"
---

# Open-Badge : badges numériques de reconnaissances et de valorisation

Open Badge est une initiative Open Source de Mozilla pour permettre une mise 
en avant des compétences via un système dit "d'achievement".
Ce système est semblable à celui que l'on peut retrouver sur divers forums, 
sites ou même consoles de jeux.

Les OpenBadges ont été pensés pour une meilleure communication de certains 
aspects plus difficile à mettre en avant sur un CV :

- on peut par exemple penser à un badge créé spécifiquement pour un évènement IT 
auquel vous avez assisté et que vous voulez présenter.
- ou encore pour avoir à porter de main une preuve virtuelle de vos divers prix, 
distinctions et certifications que vous avez pu gagner.

Pour plus d'infos, voir sur [le site officiel d'Open Badges.](https://openbadges.org/)

Schématiquement, un badge ressemble à ça : 

![Village LegalTech](../images/badge-legaltech-exemple.jpg "Village LegalTech")

Lorsqu'un badge vous est délivré, vous pouvez le stocker via [Badgr](https://info.badgr.com/)
sur lequel il vous faudra vous inscrire.
Cette solution Open Source vous permet ainsi d'avoir accès à tous vos badges, de les
regrouper sous forme de collection, de les partager sur vos réseaux, etc.

Comme vous pouvez le voir, chaque badge possède des informations comme :

- Sa description (nom, date ...)
- L'URL d'information
- Ou encore ses critères d'obtention

Ces informations sont stockées dans un fichier (en l'occurrence .Json) 
et sont liées au badge en question. 
Vous pouvez voir ce fameux fichier en cliquant 
sur le lien "Assertion" dans la description du badge.

Rien ne vaut plus que la pratique ! OpenBadges vous propose en bas de sa section
[Earning Badges](https://openbadges.org/get-started/earning-badges/) un lien
vers un exercice simple et amusant permettant de gagner un badge. Cela vous donnera une idée
de comment ce système peut être utilisé (n'oubliez pas de vous inscrire sur Badgr avant).

Si vous voulez mettre en place une telle solution, vous pouvez aussi regarder la [documentation développeurs de Badgr](https://badgr.org/).

Une alternative existe à Badgr : [Open Badges Passport](https://openbadgepassport.com/).

À vous de choisir quel sera l'outil le plus adéquat pour vous !


