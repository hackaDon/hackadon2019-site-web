---
title: "Les premiers pas"
metaTitle: "Hackadon 2020 - premiers pas"
---

Tout d'abord, bienvenue au HackaDON 2020 !

Ci-dessous une liste d'outils à votre disposition. Commencez par vous créer un compte sur Framagit puis Framateam. Une fois cela fait, 
 le responsable de votre groupe doit envoyer un message sur Framateam et on lui donnera les droits sur le dossier Google Drive et 
le repository Framagit. Elle/il pourra ensuite y inviter les autres membres du groupe !

## Framagit

On vous met à disposition un repository sur [Framagit](https://framagit.org) (solution associative basée sur Gitlab) comprenant :

* La base pour créer un site avec Gatsby (voir [les bases de Gatsby](./Les%20bases%20de%20Gatsby/1-Gatsby)) 
* Un dossier HTML si vous souhaitez partir de zéro et coder un site en pur HTML/CSS
* Un dossier Api-Platform si vous voulez aller plus loin et créer votre propre API !

Pour créer votre compte, suivez les étapes :

1. Rendez-vous sur cette page : https://framagit.org/users/sign_in  
2. Cliquez sur l'onglet "register" et créez-vous un compte.

## Framateam

Inscrivez-vous aussi sur Framateam pour avoir un espace de discussion avec les autres participants au HackaDON et votré équipe. Pour cela : 

1. Cliquez sur [ce lien](https://framateam.org/signup_user_complete/?id=5zui397n6bg73cwizbtxzfp6ah)  
2. Cliquer sur Créer un compte avec Framagit.

Une fois votre compte créé, vous devriez être redirigé vers le channel Centre-ville de Framateam qui regroupe l'ensemble des participants au HackaDON. 
Vous pouvez aussi créer des channels spécifiques à votre équipe !

C'est à ce moment que le responsable de votre groupe doit écrire sur le Centre-Ville pour qu'on lui donne les accès au Google Drive et au repository Framagit. 
On aura besoin de son email et de son identifiant Framagit :)

Une fois cela fait, elle/il pourra inviter les autres membres de l'équipe à participer au projet !

Pour info, ces channels tournent grâce à Framasoft, qui hébergent et opèrent la solution OpenSource Mattermost, sous le nom de Framateam !

## Google Drive 

Vous êtes libre d'utiliser comme bon vous semble ce dossier : y faire un carnet de bord, y présenter votre projet, utiliser les API Google, etc. 
De manière générale, essayez d'y mettre un minimum d'information pour qu'on puisse suivre les choix que vous avez pu faire
pour réaliser votre projet.

   
  
  
Bon HackaDon !!