const config = {
	"gatsby": {
		"pathPrefix": "/hackadon",
		"siteUrl": "https://hackadon.frama.io",
		"gaTrackingId": null
	},
	"header": {
		"logo": '', //done with an override from the components/images/logo.svg //"../content/images/BG-Hackadon-Badge_Numérique.svg",
		"logoLink": "https://hackadon.frama.io/hackadon/",
		"title": "Hackadon",
		"githubUrl": "https://framagit.org/hackaDon/hackadon/blob/master/content/",
		"helpUrl": "",
		"tweetText": "",
		"links": [
			{ "text": "", "link": ""}
		],
		"search": {
			"enabled": false,
			"indexName": "",
			"algoliaAppId": process.env.GATSBY_ALGOLIA_APP_ID,
			"algoliaSearchKey": process.env.GATSBY_ALGOLIA_SEARCH_KEY,
			"algoliaAdminKey": process.env.ALGOLIA_ADMIN_KEY
		}
	},
	"sidebar": {
		"forcedNavOrder": [
			"/",
		],
    	"collapsedNav": [
			  "/Technos",
    	],
		"links": [
		],
		"frontline": false,
		"ignoreIndex": true,
	},
	"siteMetadata": {
		"title": "Hackadon 2020",
		"description": "Bienvenue sur le site de présentation du Hackadon 2020",
		"ogImage": null,
		"docsLocation": "https://framagit.org/hackaDon/hackadon/blob/master/content/",
		"favicon": "https://hackadon.frama.io/hackadon/favicon.svg"
	},
};

module.exports = config;