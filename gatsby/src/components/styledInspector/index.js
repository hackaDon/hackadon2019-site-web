import {StaticQuery, graphql} from "gatsby"

import Inspector from 'react-json-inspector'
import lens from 'react-json-inspector/lib/lens'
import { navigate } from "gatsby"
import styled from "react-emotion";

/*
jaune #fded4f
bleu : #295e88
bleu avec transparence : rgba(38, 92, 138, 0.52)
*/

// props.onClick
// props.isExpanded


const StyledInspector = ({location}) => (
    <StaticQuery
      query={graphql`
        query {
          allMdx {
            edges {
              node {
                fields {
                  slug
                  title
                }
              }
            }
          }
        }
      `}

      render={render}
    />
  );

export default StyledInspector

function render({allMdx}){
    
    let rs = allMdx.edges.reduce(
        (acc, cur, idx) =>{
            
            const folders = cur.node.fields.slug.split('/')

            const recurDeep = ([node, ...others], accu) => {
                if( node === '') return recurDeep(others,accu)
                
                if (!node) return accu

                if(!accu[node]) accu[node]={}
                if(accu[node]){
                    accu[node] = recurDeep(others,accu[node])
                }

                return accu
            }

            return recurDeep(folders,acc)//acc//recurDeep(folders,acc)
        },
        {}
    )
    
    const fullRef = getFullRef(allMdx)
    const displayRef = rs

    const onClick = ({key, value, path}) => {
        
        if(Object.entries(value).length === 0 && value.constructor === Object){
            const p = `/${path.replace(/\./g,'/')}`
            navigate(p)
        }
        
        const p = path.slice(0, path.lastIndexOf('.'))
        
    }

    const isExpanded = () => {
        //console.log('in on isExpanded')
        //console.log(arguments)
        return false
    }


    return (
        <WithStyle data={ displayRef } onClick={onClick} isExpanded={isExpanded} />
    );
}


function getFullRef(allMdx){
    return allMdx.edges
}

const WithStyle = styled(props => (
    <div id='fullsidebar' style={{
        margin:'5px',
        padding:'10px 0px',
        boxShadow: 'rgb(238, 238, 238) 2px 2px 2px',
        borderRadius: '3px',
        border: '1px solid  rgb(230, 236, 241)',
        transition: 'border 200ms ease 0s'
    }} >
      <Inspector {...props} />
    </div>

  ))`
  .json-inspector,
  .json-inspector__selection {
      font: 14px/1.4 Consolas, monospace;
  }
  .json-inspector__leaf {
      padding-left: 10px;
  }
  .json-inspector__line {
      display: block;
      position: relative;
      cursor: default;
  }
  .json-inspector__leaf_root > .json-inspector__line:first-of-type {
    display: none;
  }
  .json-inspector__line:after {
      content: '';
      position: absolute;
      top: 0;
      left: -200px;
      right: -50px;
      bottom: 0;
      z-index: -1;
      pointer-events: none;
  }
  .json-inspector__line:hover >  span{
      color: #006bbf;
  }
  .json-inspector__leaf_composite > .json-inspector__line {
      cursor: pointer;
      font-size: 15px;
      font-weight: 500;
  }
  .json-inspector__radio,
  .json-inspector__flatpath {
      display: none;
  }
  .json-inspector__value {
      margin-left: 5px;
  }
  .json-inspector__search {
    min-width: 260px;
    margin: 10px 10px 10px 10px;
    padding: 2px;
    border: 1px solid #eee;
    border-radius: 5px;
  }
  .json-inspector__key {
      color: #505050;
  }
  .json-inspector__value_helper,
  .json-inspector__value_null,
  .json-inspector__not-found {
      color: #b0b0b0;
      display: none;
  }
  .json-inspector__value_string {
      color: #798953;
  }
  .json-inspector__value_boolean {
      color: #75b5aa;
  }
  .json-inspector__value_number {
      color: #d28445;
  }
  .json-inspector__hl {
      background: #ff0;
      box-shadow: 0 -1px 0 2px #ff0;
      border-radius: 2px;
  }
  .json-inspector__show-original {
      display: inline-block;
      padding: 0 6px;
      color: #666;
      cursor: pointer;
  }
  .json-inspector__show-original:hover {
      color: #111;
  }
  .json-inspector__show-original:before {
      content: '⥂';
  }
  .json-inspector__show-original:hover:after {
      content: ' expand'
  }
  `

